const wifi = require('Wifi');
const http = require('http');

const options = {
	host: 'api.altairsmartcore.com',
	path: '/streams',
	method: 'POST',
	headers: {
		'Content-Type': 'application/json',
		'Apikey': '9d715c709e137381bcfd9de6a744bb4355ac67ab70d5d1cb30e2c76104b32a40'
	}
};

function publish(data, next) {
	const body = JSON.stringify({
		protocol: 'v2',
		at: 'now',
		device: 'Kimiru@maxmedina05.maxmedina05',
		data: data
	});

	options.headers['Content-Length'] = body.length;

	var req = http.request(options, res => {
		var data = '';
		res.on('data', d => data += d);
		res.on('close', () => next(data));
	});

	req.on('error', e => console.log(e));
	req.end(body);
}

function main() {
	setInterval(() => {
		console.log('try to publish');
		publish({ soil: 0.01 }, d => console.log('Data was sent!'));
	}, 5000);
}

// eslint-disable-next-line no-unused-vars
function onInit() {
	const config = {
		ssid: 'max-iot-devices',
		password: 'fieB64!W1C'
	};

	wifi.connect(config.ssid, config, err => {

		if (err) {
			console.log('Unable to connect\n', err);
		} else {
			console.log('Connected to the Internet!');
			console.log('Network Info:', wifi.getIP());
			main();
		}
	});
}
