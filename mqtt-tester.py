#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Client paho-mqtt CarriotsMqttServer
# main.py
import paho.mqtt.publish as publish
from json import dumps


class CarriotsMqttClient():
    host = 'mqtt.altairsmartcore.com'
    port = 1883
    auth = {}
    topic = '%s/streams'
    tls = None

    def __init__(self, auth, tls=None):
        self.auth = auth
        self.topic = '%s/streams' % auth['username']
        if tls:
            self.tls = tls
            self.port = 8883

    def publish(self, msg):
        try:
            publish.single(topic=self.topic, payload=msg, hostname=self.host,
                           auth=self.auth, tls=self.tls, port=self.port)
        except Exception as ex:
            print(ex)


if __name__ == '__main__':
    auth = {'username': '9d715c709e137381bcfd9de6a744bb4355ac67ab70d5d1cb30e2c76104b32a40', 'password': ''}
    msg_dict = {'protocol': 'v2', 'device': 'Kimiru@maxmedina05.maxmedina05',
                'at': 'now', 'data': {'soil': 0.326171875}}
    # non ssl version
    client_mqtt = CarriotsMqttClient(
        auth=auth)
    client_mqtt.publish(dumps(msg_dict))
