const http = require('http');
const url = require('url');

function handleFormSubmit(req, next) {
	let rawData = '';
	req.on('data', data => rawData += data);

	req.on('end', () => {
		let form = {};

		rawData.split('&').forEach(item => {
			const pair = item.split('=');
			const key = decodeURIComponent(pair[0]);
			const value = decodeURIComponent(pair[1]);

			form[key] = value;
		});

		console.log(form);

		next();
	});
}

const proxy = http.createServer((req, res) => {
	const location = url.parse(req.url, true);

	if (location.pathname === '/network' && req.method === 'POST') {
		handleFormSubmit(req, () => {
			res.writeHead(200, { 'Content-Type': 'text/plain' });
			res.end('form submitted');
		})
	} else {
		res.writeHead(200, { 'Content-Type': 'text/plain' });
		res.end('okay');
	}
});

proxy.listen(8080, '127.0.0.1', () => {
	console.log('listening on port 8080');
});