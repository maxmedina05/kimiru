/**
 * Using the Y-69 and Y38 sensor
 * Wet: Output voltage decreases. It gets close to 0
 * Dry: Output voltage increaes. It gets close to 1
 */
function measureSoil() {
	const value = analogRead(A0);
	
	if (value >= 0.95 || value <= 0.05) {
		console.log('sensor is disconnected!');
	}

	console.log(`Soil: ${value}`);
}

// eslint-disable-next-line no-unused-vars
function onInit() {
	setInterval(() => measureSoil(), 1000);
}
