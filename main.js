const fs = require('Storage');
const wifi = require('Wifi');
const PASS_FILE = '.passwd';
const http = require('http');

const options = {
	host: 'api.altairsmartcore.com',
	path: '/streams',
	method: 'POST',
	headers: {
		'Content-Type': 'application/json',
		'Apikey': '9d715c709e137381bcfd9de6a744bb4355ac67ab70d5d1cb30e2c76104b32a40'
	}
};

function onError(err) {
	console.log(err);
	digitalWrite(D2, false);
}

function setupServer() {
	function handleForm(req, next) {
		let data = '';
		req.on('data', d => data += d);

		req.on('end', () => {
			let form = {};
			data.split('&').forEach(e => {
				const arr = e.split('=');
				const k = decodeURIComponent(arr[0]);
				const v = decodeURIComponent(arr[1]);

				form[k] = v;
			});

			const file = fs.read(PASS_FILE);
			let newObj = {};

			if (file) {
				const obj = JSON.parse(file);
				const exists = obj.networks.indexOf(form.ssid) !== -1;
				const newNetworks = exists ? obj.networks : obj.networks.concat([form.ssid]);

				Object.assign(newObj, obj);
				newObj['networks'] = newNetworks;
				newObj[form.ssid] = form.password;

			} else {
				newObj['networks'] = [form.ssid];
				newObj[form.ssid] = form.password;
			}

			fs.write(PASS_FILE, newObj);

			setTimeout(() => require('ESP8266').reboot(), 3000);
			next();
		});
	}

	http.createServer((req, res) => {
		const loc = url.parse(req.url, true);

		if (loc.pathname === '/') {
			const file = fs.read('idx.html');
			res.writeHead(200, { 'Content-Type': 'text/html' });
			res.end(file);

		} else if (loc.pathname === '/networks' && req.method === 'POST') {
			handleForm(req, () => {
				res.writeHead(200, { 'Content-Type': 'text/plain' });
				res.end('Success!. Server will restart now.');
			});

		} else {
			res.writeHead(404, { 'Content-Type': 'text/plain' });
			res.end(`404 Error: Page at ${loc.pathname} not found!`);
		}

	}).listen(8080);

	console.log('Server Listening on port:', 8080);
	wifi.disconnect();
	wifi.startAP('Espruino Kimiru');
	digitalWrite(D2, true);
}

function getAPs(next) {
	wifi.scan(list => {
		const file = fs.read(PASS_FILE);

		if (!list || !file) {
			return next([]);
		}

		const obj = JSON.parse(file);
		const aps = list.filter(e => !!obj[e.ssid]).map(e => ({ ssid: e.ssid, password: obj[e.ssid] }));

		obj.networks.forEach(ssid => aps.push({ ssid: ssid, password: obj[ssid] }));
		next(aps);
	});
}

function connect(config, next) {
	if (config && config.ssid && config.password) {
		wifi.connect(config.ssid, config, err => {
			if (err) {
				console.log('Unable to connect\n', err);
				next(err);

			} else {
				console.log('Connected to the Internet!');
				console.log('Network Info:', wifi.getIP());
				wifi.setHostname('espruino_kimiru');
				wifi.save();
				wifi.stopAP();
				next();
			}
		});
	} else {
		next('Invalid parameters!');
	}
}

function retry(idx, limit, networks) {
	if (idx < limit) {
		connect(networks[idx], err => {
			if (err) {
				retry(idx + 1, limit, networks);
			} else {
				main();
			}
		});
	} else {
		setupServer();
	}
}

function publish(data, next) {
	const body = JSON.stringify({
		protocol: 'v2',
		at: 'now',
		device: 'Kimiru@maxmedina05.maxmedina05',
		data: data
	});

	options.headers['Content-Length'] = body.length;

	var req = http.request(options, res => {
		var data = '';
		res.on('data', d => data += d);
		res.on('close', () => next('', data));
	});

	req.on('error', e => next(e, undefined));
	req.end(body);
}

/**
 * Using the Y-69 and Y38 sensor
 * Wet: Output voltage decreases. It gets close to 0
 * Dry: Output voltage increaes. It gets close to 1
 */
function measure(next) {
	const value = analogRead(A0);

	if (value >= 0.95 || value <= 0.1) {
		return next('Sensor is disconnected!', value);
	}

	next('', { soil: value });
}

// eslint-disable-next-line no-unused-vars
function onInit() {
	digitalWrite(D2, true);

	wifi.getDetails(details => {
		if (details.status === 'connected') {
			console.log('Network Info:', wifi.getIP());
			main();

		} else {
			getAPs(networks => retry(0, networks.length, networks));
		}
	});
}

function main() {
	console.log('Program Running!');
	setInterval(() => measure((err, data) => {
		if (err) {
			onError(err);
		} else {
			publish(data, e => !!e && onError(e));
		}
	}), 1800000);
}
